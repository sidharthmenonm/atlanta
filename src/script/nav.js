document.querySelector("#navbar .menu-btn").addEventListener('click', function() {
  document.querySelector("#navbar").classList.add('open');
  document.querySelector("#aside").classList.add('open');
});

document.querySelector("#aside .overlay").addEventListener('click', function() {
  document.querySelector("#navbar").classList.remove('open');
  document.querySelector("#aside").classList.remove('open');
})