import AOS from 'aos';
import 'aos/dist/aos.css';

import Swiper, { Navigation, Pagination, EffectCoverflow, Autoplay } from 'swiper';
Swiper.use([Navigation, Pagination, EffectCoverflow, Autoplay]);
import 'swiper/swiper-bundle.css';

AOS.init();

var swiper = new Swiper('.swiper-container', {
  effect: 'coverflow',
  grabCursor: true,
  centeredSlides: true,
  slidesPerView: 'auto',
  loop: true,
  autoplay: true,
  coverflowEffect: {
    rotate: 50,
    stretch: 0,
    depth: 150,
    modifier: 1,
    slideShadows: true,
  },
  pagination: {
    el: '.swiper-pagination',
  },
});